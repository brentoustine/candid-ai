import React, { Component } from 'react';
import Clarifai from 'clarifai';
import './App.css';
import Navigation from './components/Navigation/Navigation';
import ImageLinkForm from './components/ImageLinkForm/ImageLinkForm';
import FaceRecognition from './components/FaceRecognition/FaceRecognition';
import Logo from './components/Logo/Logo';
import Rank from './components/Rank/Rank';
import ParticlesBg from 'particles-bg';

const app = new Clarifai.App({
  apiKey: "500647f07301423d9f6282362fab4958",
 });
  

class App extends Component {
  constructor() {
    super();
    this.state = {
      input: '',
      imageUrl: '',
    }
  }

  onInputChange = (event) => {
    this.setState({input: event.target.value});
  }

  onButtonSubmit = () => {
   this.setState({imageUrl: this.state.input})
    app.models
   .predict(
    'f76196b43bbd45c99b4f3cd8e8b40a8a',
   this.state.input).then(
    function(response) {
      console.log(response.outputs[0].data.regions[0].region_info.bounding_box);
    },
    function(err){
      console.log()
    }
   )
   
   };   

  render() {
    return (
      <div className="App">
        <Navigation />
        <Logo />
        <h1>Candid-AI</h1>
        <Rank />
        <ImageLinkForm onInputChange={this.onInputChange} onButtonSubmit={this.onButtonSubmit} />
        <FaceRecognition imageUrl={this.state.imageUrl} />
        {/* <ParticlesBg type="circle" className='particles' bg={true}/> */}
      </div>
      
    );

  }
  
}

export default App;
