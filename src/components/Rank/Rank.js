import React from 'react'

const Rank = () => {
  return (
    <div>
        <div className='white f3'>
          {'Francis, your current rank is...'}
        </div>

        <div className='white f1'>
          {'Top: 1'}
        </div>
    </div>
  )
}

export default Rank