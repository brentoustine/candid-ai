import React from 'react'
import Tilt from 'react-parallax-tilt';
import Face from './Face.png';
import './Logo.css'

const Logo = () => {
  return (
    <div className='ma4 mt0'>
        <Tilt className='Tilt br2 shadow-2' style={{ height: 150, width: 150, backgroundColor: 'skyblue' }}>    
        <h1 className='Tilt-inner pa3'><img style={{paddingTop: '5px'}} alt='Logo 'src={Face}/></h1> 
        </Tilt>
    </div>
    
  )
}

export default Logo;